import pandas as pd
import random
from math import floor
import os


def remove_instances_without_data(data_path, file_name, subsets, sep=","):
    """
    This function cleans the data by dropping empty cells in a dataset.
    Useful for general cleaning of missing data.
    :param subsets: names of columns where the function checks for empty cells
    :type subsets: list of strings
    """
    df_data = pd.read_csv(data_path, sep=sep)
    begin = len(df_data)
    for sub in subsets:
        df_data = df_data.dropna(subset=[sub])
    df_data.to_csv(file_name, sep=sep, index=False)
    print("Removed instances: " + str(begin - len(df_data)))


def duplicate_instances_without_labels(data_path, file_name):
    df_data = pd.read_csv(data_path)

    id_labels_dict = {}
    for index, row in df_data.iterrows():
        if not pd.isnull(row['vrsta']):
            id_labels_dict[row['ID']] = row['vrsta']

    for index, row in df_data.iterrows():
        if pd.isnull(row['vrsta']):
            row['vrsta'] = id_labels_dict[row['ID']]

    df_data.to_csv(file_name, index=False)


def create_IT_folds(data_path, folds_path):
    data = pd.read_csv(data_path)
    for i in range(8):
        annotator = 'IMSyPP_IT_YouTube_a' + str(i) + '.csv'
        train_fold_name = 'train_IT_' + str(i)
        test_fold_name = 'test_IT_' + str(i)
        train_fold_path = os.path.join(folds_path, train_fold_name)
        test_fold_path = os.path.join(folds_path, test_fold_name)
        test_data = data.loc[data['Annotator'] == annotator]
        train_data = data.loc[data['Annotator'] != annotator]
        print(len(train_data))
        threads = {}
        for index, row in test_data.iterrows():
            if row['ID_Video'] not in threads:
                threads[row['ID_Video']] = 1

        for k, v in threads.items():
            train_data = train_data.loc[train_data['ID_Video'] != k]
        print(len(train_data))
        print(len(test_data))
        print("\n")
        train_data.to_csv(train_fold_path, index=False)
        test_data.to_csv(test_fold_path, index=False)


def create_EN_folds(data_folder, folds_folder):
    annotators = ['IMSyPP_EN_YouTube_a0 (AS).csv', 'IMSyPP_EN_YouTube_a1 (AT).csv', 'IMSyPP_EN_YouTube_a2 (MG).csv',
                  'IMSyPP_EN_YouTube_a3 (HA).csv', 'IMSyPP_EN_YouTube_a4 (JZ).csv', 'IMSyPP_EN_YouTube_a5 (KB).csv',
                  'IMSyPP_EN_YouTube_a6 (NS).csv', 'IMSyPP_EN_YouTube_a7 (NC).csv', 'IMSyPP_EN_YouTube_a8 (PP).csv',
                  'IMSyPP_EN_YouTube_a9 (ZF).csv']

    frames = []
    for a in annotators:
        dataset = os.path.join(data_folder, a)
        d = pd.read_csv(dataset)
        d = d.dropna(subset=['Comment_ID'])
        d = d.dropna(subset=['Type'])   #this is potentially problematic
        d['Annotator'] = a
        frames.append(d)

    data = pd.concat(frames)
    for i, annotator in enumerate(annotators):
        #annotator = 'IMSyPP_IT_YouTube_a' + str(i) + '.csv'
        train_fold_name = 'train_EN_' + str(i)
        test_fold_name = 'test_EN_' + str(i)
        train_fold_path = os.path.join(folds_folder, train_fold_name)
        test_fold_path = os.path.join(folds_folder, test_fold_name)
        test_data = data.loc[data['Annotator'] == annotator]
        train_data = data.loc[data['Annotator'] != annotator]
        print(len(train_data))

        # odstranjujuemo tiste komentarje iz učne množice, ki se nahajajo v testni (ker jih je tudi
        # testni anotator označil)
        threads = {}
        for index, row in test_data.iterrows():
            if row['Video_ID'] not in threads:
                threads[row['Video_ID']] = 1

        for k, v in threads.items():
            train_data = train_data.loc[train_data['Video_ID'] != k]
        print(len(train_data))
        print(len(test_data))
        print("\n")
        train_data.to_csv(train_fold_path, index=False)
        test_data.to_csv(test_fold_path, index=False)


def clean_and_split_pretraining_dataset(data_path_pretraining, data_path_training, num_instances, split_per):
    data_pretraining = pd.read_csv(data_path_pretraining, encoding='utf-16')
    data_training = pd.read_csv(data_path_training)
    threads = {}
    for index, row in data_training.iterrows():
        if row['ID_Video'] not in threads:
            threads[row['ID_Video']] = 1

    filtered_data = []
    for index, row in data_pretraining.iterrows():
        if row['ID_Video'] not in threads:
            filtered_data.append(row['Testo'])

    print(len(data_pretraining))
    print(len(filtered_data))

    subsample = random.sample(filtered_data, num_instances)
    eval_data = subsample[:floor(num_instances * split_per)]
    train_data = subsample[floor(num_instances * split_per):]

    train_df = pd.DataFrame(data=train_data, columns=["Testo"])
    eval_df = pd.DataFrame(data=eval_data, columns=["Testo"])

    train_df.to_csv("../../data/pretraining_train.csv")
    eval_df.to_csv("../../data/pretraining_eval.csv")


def correct_labels_EN_folds(data_folder):
    """
    One of the annotators was very adamant in using a false label for the appropriate class (false label: 0, 
    correct label: 0. appropriate). This function corrects this in the prepared crossvalidation folds
    "
    :return: void
    """
    for i in range(10):
        print("Correcting labels for the the split number " + str(i) + "...")
        train_data_path = "train_EN_" + str(i)
        train_data_path = os.path.join(data_folder, train_data_path)
        test_data_path = "test_EN_" + str(i)
        test_data_path = os.path.join(data_folder, test_data_path)
        data = pd.read_csv(train_data_path, dtype="object")
        #print(data)
        for index, row in data.iterrows():
            if row['Type'] == "0":
                row['Type'] = "0. appropriate"
        data.to_csv(train_data_path+"_corrected", index=False)
        data = pd.read_csv(test_data_path, dtype="object")
        # print(data)
        test_labels = data['Type'].tolist()
        for index, row in data.iterrows():
            if row['Type'] == "0":
                row['Type'] = "0. appropriate"
        data.to_csv(test_data_path + "_corrected", index=False)


def clean_EN_round1_data():
    """A function that cleans the EN Youtube round 1 data (unsplit).
    """
    data_path = "/home/andrazp/context_based_hs/data/EN YouTube/IMSyPP_EN_YouTube_comments_round1.csv"
    data = pd.read_csv(data_path, dtype="object")
    data = data.dropna(subset=['Comment_ID'])
    data = data.dropna(subset=['Type'])
    for index, row in data.iterrows():
        if row['Type'] == "0":
            row['Type'] = "0. appropriate"
    data.to_csv(data_path + "_cleaned", index=False)


if __name__ == "__main__":
    #remove_instances_without_data("../../data/IMSyPP_IT_YouTube_comments_round1.csv",
    #                                "../../data/IMSyPP_IT_YouTube_comments_round1_cleaned.csv",
    #                              ['Tipo', "Testo"])
    #create_IT_folds("../../data/IMSyPP_IT_YouTube_comments_round1_cleaned.csv",
    #                "../../data/IT_folds")
    #clean_and_split_pretraining_dataset("../../data/comments_it_preprocessing.csv",
    #                                    "../../data/IMSyPP_IT_YouTube_comments_round1_cleaned.csv",
    #                                    100000, 0.2)
    #create_EN_folds("../../data/EN YouTube/", "../../data/EN_folds")
    #clean_EN_round1_data()
    remove_instances_without_data("../../data/IMSyPP_IT_YouTube_comments_round2.csv",
                                    "../../data/IMSyPP_IT_YouTube_comments_round2_cleaned.csv",
                                  ['Tipo', "Testo"])