import pandas as pd

def write_outputs(original_data, predictions, probabilities, output_filename):
    """Writes outputs of the model next to the data and gold labels ans saves it to a new file."""
    prob_0 = []
    prob_1 = []
    prob_2 = []
    prob_3 = []

    for probs in probabilities:
        prob_0.append(probs[0])
        prob_1.append(probs[1])
        prob_2.append(probs[2])
        prob_3.append(probs[3])

    original_data['model predictions'] = predictions
    original_data['0_ni_sovrazni'] = prob_0
    original_data['1_nespodobni'] = prob_1
    original_data['2_zalitev'] = prob_2
    original_data['3_nasilje'] = prob_3

    original_data.to_csv(output_filename, sep="\t", index=False)


def write_outputs_binary(original_data, predictions, probabilities, output_filename):
    """Writes outputs of the model next to the data and gold labels ans saves it to a new file."""
    prob_0 = []
    prob_1 = []

    for probs in probabilities:
        prob_0.append(probs[0])
        prob_1.append(probs[1])

    original_data['model predictions'] = predictions
    original_data['0'] = prob_0
    original_data['1'] = prob_1

    original_data.to_csv(output_filename, sep="\t", index=False)
