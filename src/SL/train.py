#from bert_ml_sentiment_classifier import bert_train, bert_evaluate
from MLBertModelForClassification import BertClassificationTraining
from data_transformation import encode_labels, prepare_data_for_training, prepare_data_for_testing

from transformers import BertTokenizer, BertForSequenceClassification, BertConfig
from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score
import torch
import pandas as pd
import numpy as np

import random
import argparse
import os
#import csv
from math import floor


def run():
    parser = argparse.ArgumentParser()

    parser.add_argument("--train_data_path",
                        required=True,
                        type=str)
    parser.add_argument("--output_dir",
                        required=True,
                        type=str)

    parser.add_argument("--tokenizer_file",
                        type=str)
    parser.add_argument("--config_file",
                        type=str)
    parser.add_argument("--model_file",
                        type=str)

    parser.add_argument("--eval_split",
                        default=0.2,
                        type=float)
    parser.add_argument("--test_split",
                        default=0.1,
                        type=float)
    parser.add_argument("--max_len",
                        default=512,
                        type=int)
    parser.add_argument("--batch_size",
                        default=32,
                        type=int)
    parser.add_argument("--num_epochs",
                        default=3,
                        type=int)
    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float)
    parser.add_argument("--weight_decay",
                        default=0.01,
                        type=float)
    parser.add_argument("--warmup_proportion",
                        default=0.1,
                        type=float)
    parser.add_argument("--adam_epsilon",
                        default=1e-8,
                        type=float)
    parser.add_argument("--random_seed",
                        default=42,
                        type=int)

    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)

    print("Setting the random seed...")
    random.seed(args.random_seed)
    np.random.seed(args.random_seed)
    torch.manual_seed(args.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    print("Reading data...")
    df_data = pd.read_csv(args.train_data_path, dtype={'ID': object}, encoding="utf-8")
    df_data = df_data.sort_values(by=['ID'])
    print(df_data)
    data = df_data['besedilo'].tolist()
    label_set = sorted(list(set(df_data['vrsta'].values)))
    labels = encode_labels(df_data['vrsta'].tolist(), label_set)
    num_labels = len(set(labels))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    print("Loading pre-trained model...")
    tokenizer_file = args.tokenizer_file
    config_file = args.config_file
    model_file = args.model_file
    # Value in not None put empty string
    if tokenizer_file is not None:
        if len(args.tokenizer_file) == 0:
            tokenizer_file = None

    if config_file is not None:
        if len(args.config_file) == 0:
            config_file = None

    if model_file is not None:
        if len(args.model_file) == 0:
            model_file = None

    if tokenizer_file is not None:
        tokenizer = BertTokenizer.from_pretrained(tokenizer_file, do_lower_case=False)
    else:
        tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)

    if config_file is not None and model_file is not None:
        config = BertConfig.from_pretrained(config_file, num_labels=len(label_set))
        model = BertForSequenceClassification.from_pretrained(model_file, config=config)
    else:
        model = BertForSequenceClassification.from_pretrained('bert-base-multilingual-cased',
                                                              num_labels=len(label_set))

    bert_trainer = BertClassificationTraining(model, device, tokenizer, batch_size=args.batch_size,
                                               lr=args.learning_rate, train_epochs=args.num_epochs,
                                               weight_decay=args.weight_decay,
                                               warmup_proportion=args.warmup_proportion,
                                               adam_epsilon=args.adam_epsilon)

    print("Preparing data...")

    eval_data = data[(floor(len(data) * 9 * 0.1)):(floor(len(data) * (9 + 1) * 0.1))]
    eval_labels = labels[floor((len(labels) * 9 * 0.1)):floor((len(labels) * (9 + 1) * 0.1))]
    train_data = data[:floor((len(data) * 9 * 0.1))] + data[floor((len(data) * (9 + 1) * 0.1)):]
    train_labels = labels[:floor((len(labels) * 9 * 0.1))] + labels[floor((len(labels) * (9 + 1) * 0.1)):]

    print("Train label:")
    print(train_labels[0])
    print("Train data:")
    print(train_data[0])
    train_dataloader = prepare_data_for_training(train_data, train_labels, tokenizer, args.max_len, args.batch_size)
    eval_dataloader = prepare_data_for_training(eval_data, eval_labels, tokenizer, args.max_len, args.batch_size)

    print("Training...")
    bert_trainer.train(train_dataloader, eval_dataloader, output_dir, save_best=True)
    print("Done.")


if __name__ == "__main__":
    run()
