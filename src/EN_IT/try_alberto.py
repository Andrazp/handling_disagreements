import torch
from tokenizer import *
#from transformers import AutoTokenizer, AutoModel
from transformers import BertForSequenceClassification, BertConfig

print("Loading pre-trained model...")
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

t = AlBERToTokenizer(do_lower_case=True,vocab_file="../../models/alberto/vocab.txt", do_preprocessing=True)
#tokens = tok.tokenize(b)
#print(tokens)

#model = AutoModel.from_pretrained("m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0",
#                                  num_labels=num_labels)
config = BertConfig.from_pretrained("../../models/alberto/config.json", num_labels=2, output_hidden_states=True,
                                                      output_attentions=True)
model = BertForSequenceClassification.from_pretrained("../../models/alberto/pytorch_model.bin", config=config
                                                      )

tokenized_sentence = t.encode("Let's see all hidden-states and attentions on this text. Or maybe just maybe not.")
input_ids = torch.tensor([t.encode("Let's see all hidden-states and attentions on this text. Or maybe just maybe not.")])
print(input_ids)
#input_ids = torch.tensor([tokenizer.encode("Let's see all hidden-states and attentions on this text. Or maybe just maybe not.")])
#input_ids.append(torch.tensor([tokenizer.encode("I have no fucking idea what to write here.")]))
outputs = model(input_ids)
print("Type: %s" % type(outputs))
print("Size: %d" % len(outputs))
all_hidden_states, all_attentions = outputs[-2:]
print(type(all_hidden_states))
print(len(all_hidden_states))
print(all_hidden_states[0].size())
print(len(all_attentions))
print(all_attentions[0].size())
print(all_attentions[0])