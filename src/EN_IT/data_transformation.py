import torch
from torch.utils.data import TensorDataset, RandomSampler, SequentialSampler, DataLoader
from keras_preprocessing.sequence import pad_sequences

#import math
import sys

def encode_labels(labels, labels_set):
    """Maps each label to a unique index.
    DEPRECATED
    :param labels: (list of strings) labels of every instance in the dataset
    :param labels_set: (list of strings) set of labels that appear in the dataset
    :return (list of int) encoded labels
    """
    encoded_labels = []
    for label in labels:
        encoded_labels.append(labels_set.index(label))
    return encoded_labels


def encode_labels_consistent(labels):
    """Maps each label to a unique index.
    TO USE IN ALL FUTURE EXPERIMENTS
    :param labels: (list of strings) labels of every instance in the dataset
    :return (list of int) encoded labels
    """
    encoded_labels = []
    for label in labels:
        if label == "negative":
            encoded_labels.append(0)
        elif label == "neutral":
            encoded_labels.append(1)
        elif label == "positive":
            encoded_labels.append(2)
        else:
            print("Wrong label in the dataset.")
            sys.exit()
    return encoded_labels


def relabel_data_to_binary(labels, label_set_1, label_set_2):
    relabeled = []
    for l in labels:
        if l in label_set_1:
            relabeled.append(0)
        elif l in label_set_2:
            relabeled.append(1)
        else:
            raise ValueError('Found a label in the dataset which does not belong to any subset.')
    return relabeled


def prepare_data_for_training(data, labels, tokenizer, max_len, batch_size):
    """REFACTOR: MAKE TWO FUNCTIONS - ONE PREPARES DATA, THE OTHER CREATES A DATALOADER"""
    #sentences = ["[CLS] " + sentence + " [SEP]" for sentence in data]

    tokenized_sentences = [tokenizer.tokenize(sentence) for sentence in data]
    truncated_sentences = [sentence[:(max_len - 2)] for sentence in tokenized_sentences]
    truncated_sentences = [["[CLS]"] + sentence + ["[SEP]"] for sentence in truncated_sentences]
    print("Example of tokenized sentence:")
    print(truncated_sentences[0])

    input_ids = [tokenizer.convert_tokens_to_ids(sentence) for sentence in truncated_sentences]
    print("Printing encoded sentences:")
    print(input_ids[0])
    # dtype must be long because BERT apparently expects it
    input_ids = pad_sequences(input_ids, dtype='long', maxlen=max_len, padding="post", truncating="post")

    # attention masks
    attention_masks = []
    for seq in input_ids:
        seq_mask = [float(i > 0) for i in seq]
        attention_masks.append(seq_mask)

    input_ids = torch.tensor(input_ids)
    labels = torch.tensor(labels)
    attention_masks = torch.tensor(attention_masks)

    transformed_data = TensorDataset(input_ids, attention_masks, labels)
    sampler = RandomSampler(transformed_data)
    dataloader = DataLoader(transformed_data, sampler=sampler, batch_size=batch_size)

    return dataloader


def prepare_data_for_testing(data, tokenizer, max_len, batch_size):
    """REFACTOR: MAKE TWO FUNCTIONS - ONE PREPARES DATA, THE OTHER CREATES A DATALOADER"""
    #sentences = ["[CLS] " + sentence + " [SEP]" for sentence in data]

    tokenized_sentences = [tokenizer.tokenize(sentence) for sentence in data]
    truncated_sentences = [sentence[:(max_len - 2)] for sentence in tokenized_sentences]
    truncated_sentences = [["[CLS]"] + sentence + ["[SEP]"] for sentence in truncated_sentences]
    print("Example of tokenized sentence:")
    print(truncated_sentences[0])

    input_ids = [tokenizer.convert_tokens_to_ids(sentence) for sentence in truncated_sentences]
    print("Printing encoded sentences:")
    print(input_ids[0])
    # dtype must be long because BERT apparently expects it
    input_ids = pad_sequences(input_ids, dtype='long', maxlen=max_len, padding="post", truncating="post")

    # attention masks
    attention_masks = []
    for seq in input_ids:
        seq_mask = [float(i > 0) for i in seq]
        attention_masks.append(seq_mask)

    input_ids = torch.tensor(input_ids)
    attention_masks = torch.tensor(attention_masks)

    transformed_data = TensorDataset(input_ids, attention_masks)
    sampler = SequentialSampler(transformed_data)
    dataloader = DataLoader(transformed_data, sampler=sampler, batch_size=batch_size)

    return dataloader
