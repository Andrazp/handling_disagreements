#from bert_ml_sentiment_classifier import bert_train, bert_evaluate
from BertClassificationTraining import BertClassificationTraining
from data_transformation import encode_labels, prepare_data_for_training, prepare_data_for_testing
from tokenizer import *


from transformers import BertTokenizer, BertForSequenceClassification, BertConfig
from sklearn.model_selection import train_test_split
from sklearn.metrics import recall_score
import torch
import pandas as pd
import numpy as np

import random
import argparse
import os
import sys
import math


def run():
    parser = argparse.ArgumentParser()

    parser.add_argument("--train_data_file",
                        required=True,
                        type=str)
    parser.add_argument("--output_dir",
                        required=True,
                        type=str)
    parser.add_argument("--model",
                        type=str,
                        default="alberto")

    parser.add_argument("--val_data_file",
                        type=str)
    parser.add_argument("--max_len",
                        default=256,
                        type=int)
    parser.add_argument("--batch_size",
                        default=32,
                        type=int)
    parser.add_argument("--num_epochs",
                        default=3,
                        type=int)
    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float)
    parser.add_argument("--weight_decay",
                        default=0.01,
                        type=float)
    parser.add_argument("--warmup_proportion",
                        default=0.1,
                        type=float)
    parser.add_argument("--adam_epsilon",
                        default=1e-8,
                        type=float)
    parser.add_argument("--random_seed",
                        default=42,
                        type=int)

    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)

    print("Setting the random seed...")
    random.seed(args.random_seed)
    np.random.seed(args.random_seed)
    torch.manual_seed(args.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    print("Loading data...")
    if args.val_data_file is not None:
        train_data_path = args.train_data_file
        val_data_path = args.val_data_file
        data = pd.read_csv(train_data_path)
        print(data)
        train_data = data['Text'].tolist()
        label_set = sorted(list(set(data['Type'].values)))
        train_labels = encode_labels(data['Type'].tolist(), label_set)
        num_labels = len(set(train_labels))
        data = pd.read_csv(val_data_path)
        val_data = data['Text'].tolist()
        val_labels = encode_labels(data['Type'].tolist(), label_set)
    else:
        train_data_path = args.train_data_file
        data = pd.read_csv(train_data_path)
        print(data)
        label_set = sorted(list(set(data['Type'].values)))
        train_set = data[:math.floor(len(data) * 0.9)]
        val_set = data[math.floor(len(data) * 0.9):]
        print(len(train_set))
        print(len(val_set))
        train_data = train_set['Text'].tolist()
        train_labels = encode_labels(train_set['Type'].tolist(), label_set)
        num_labels = len(set(train_labels))
        val_data = val_set['Text'].tolist()
        val_labels = encode_labels(val_set['Type'].tolist(), label_set)

    print("Loading pre-trained model...")
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    if args.model == "alberto":
        t = AlBERToTokenizer(do_lower_case=True, vocab_file="../../models/alberto/vocab.txt", do_preprocessing=True)
        config = BertConfig.from_pretrained("../../models/alberto/config.json", num_labels=len(label_set))
        model = BertForSequenceClassification.from_pretrained("../../models/alberto/pytorch_model.bin", config=config)
    elif args.model == "mbert":
        t = BertTokenizer.from_pretrained('bert-base-multilingual-cased')
        model = BertForSequenceClassification.from_pretrained('bert-base-multilingual-cased', num_labels=num_labels)
    elif args.model == "bert-base":
        t = BertTokenizer.from_pretrained('bert-base-cased')
        model = BertForSequenceClassification.from_pretrained('bert-base-cased', num_labels=num_labels)
    else:
        print("Please specify a valid pretraine model: mbert, alberto, bert-base")
        sys.exit()

    bert_trainer = BertClassificationTraining(model, device, t, batch_size=args.batch_size,
                                               lr=args.learning_rate, train_epochs=args.num_epochs,
                                               weight_decay=args.weight_decay,
                                               warmup_proportion=args.warmup_proportion,
                                               adam_epsilon=args.adam_epsilon)

    print("Train label:")
    print(train_labels[0])
    print("Train data:")
    print(train_data[0])
    train_dataloader = prepare_data_for_training(train_data, train_labels, t, args.max_len, args.batch_size)
    val_dataloader = prepare_data_for_training(val_data, val_labels, t, args.max_len, args.batch_size)

    print("Training...")
    bert_trainer.train(train_dataloader, val_dataloader, output_dir, save_best=True)
    print("Done.")


if __name__ == "__main__":
    run()
