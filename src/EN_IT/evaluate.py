#from bert_ml_sentiment_classifier import bert_train, bert_evaluate
from BertClassificationTraining import BertClassificationTraining
from data_transformation import encode_labels, prepare_data_for_training, prepare_data_for_testing
from utils import write_outputs
from krippendorf import IntervalKAlpha

from transformers import BertTokenizer, BertForSequenceClassification, BertConfig
from tokenizer import *
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, f1_score, recall_score, confusion_matrix
import torch
import pandas as pd
import numpy as np

import random
import argparse
import os
#import csv
from math import floor


def evaluate():
    parser = argparse.ArgumentParser()

    parser.add_argument("--train_data_path",
                        required=True,
                        type=str)
    parser.add_argument("--model_dir",
                        required=True,
                        type=str)
    parser.add_argument("--results_file",
                        required=True,
                        type=str)

    parser.add_argument("--tokenizer_file",
                        type=str)

    parser.add_argument("--eval_split",
                        default=0.2,
                        type=float)
    parser.add_argument("--test_split",
                        default=0.1,
                        type=float)
    parser.add_argument("--max_len",
                        default=512,
                        type=int)
    parser.add_argument("--batch_size",
                        default=32,
                        type=int)
    parser.add_argument("--num_epochs",
                        default=3,
                        type=int)
    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float)
    parser.add_argument("--weight_decay",
                        default=0.01,
                        type=float)
    parser.add_argument("--warmup_proportion",
                        default=0.1,
                        type=float)
    parser.add_argument("--adam_epsilon",
                        default=1e-8,
                        type=float)

    args = parser.parse_args()
    alpha_metric = IntervalKAlpha()

    log_path = os.path.join(args.model_dir, "evaluation_set_testing")

    print("Reading data...")
    df_data = pd.read_csv(args.train_data_path, encoding="utf-8")
    df_data = df_data.dropna(subset=['Type'])
    df_data = df_data.dropna(subset=['Text'])
    data = df_data['Text'].tolist()
    label_set = sorted(list(set(df_data['Type'].values)))
    print(label_set)
    labels = encode_labels(df_data['Type'].tolist(), label_set)
    #num_labels = len(set(labels))

    print("Evaluating model...")
    current_dir = args.model_dir
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)

    #print("Loading pre-trained model...")
    #tokenizer_file = args.tokenizer_file

    # Value in not None put empty string
    #if tokenizer_file is not None:
    #    if len(args.tokenizer_file) == 0:
    #        tokenizer_file = None

    #if args.tokenizer_file is not None:
    #    t = BertTokenizer.from_pretrained(args.tokenizer_file, do_lower_case=False)
    #else:
    #    t = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
    #t = AlBERToTokenizer(do_lower_case=True, vocab_file="../../models/alberto/vocab.txt", do_preprocessing=True)
    t = BertTokenizer.from_pretrained('bert-base-cased', do_lower_case=False)
    config = BertConfig.from_pretrained(os.path.join(current_dir, "config.json"), num_labels=len(label_set))
    model = BertForSequenceClassification.from_pretrained(os.path.join(current_dir, "pytorch_model.bin"),
                                                          config=config)

    bert_trainer = BertClassificationTraining(model, device, t, batch_size=args.batch_size,
                                               lr=args.learning_rate, train_epochs=args.num_epochs,
                                               weight_decay=args.weight_decay,
                                               warmup_proportion=args.warmup_proportion,
                                               adam_epsilon=args.adam_epsilon)

    print("Preparing data...")
    test_dataloader = prepare_data_for_testing(data, t, args.max_len, args.batch_size)

    predictions, probabilities = bert_trainer.predict(test_dataloader, return_probabilities=True)

    cm = confusion_matrix(labels, predictions)
    alpha = alpha_metric.alpha_score(cm)
    print(cm)
    print(accuracy_score(labels, predictions))
    print("Interval Alpha: {}".format(alpha))
    print(f1_score(labels, predictions, average='micro'))
    print(f1_score(labels, predictions, average='macro'))
    f1_scores = f1_score(labels, predictions, average=None)
    print("F1 '0 ni: '" + str(f1_scores[0]))
    print("F1 '1 nespodobni: '" + str(f1_scores[1]))
    print("F1 '2 žalitev: '" + str(f1_scores[2]))
    print("F1 '3 nasilje: '" + str(f1_scores[3]))
    print(recall_score(labels, predictions, labels=[3], average=None)[0])
    write_outputs(df_data, predictions, probabilities, args.results_file)
    with open(log_path, 'a') as f:
        f.write(np.array2string(confusion_matrix(labels, predictions), separator=', '))
        f.write("ACC: " + str(accuracy_score(labels, predictions)))
        f.write("Interval Alpha: " + str(alpha))
        f.write("Micro F-1: " + str(f1_score(labels, predictions, average='micro')))
        f.write("Macro F-1 " + str(f1_score(labels, predictions, average='macro')))
        f.write("F1 '0 ni: '" + str(f1_scores[0]))
        f.write("F1 '1 nespodobni: '" + str(f1_scores[1]))
        f.write("F1 '2 žalitev: '" + str(f1_scores[2]))
        f.write("F1 '3 nasilje: '" + str(f1_scores[3]))
        f.write("Recall (min): " + str(recall_score(labels, predictions, labels=[3], average=None)[0]))
    print("Done.")


if __name__ == "__main__":
    evaluate()
