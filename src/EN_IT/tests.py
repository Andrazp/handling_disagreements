from data_transformation import prepare_data_for_testing, relabel_data_to_binary
import pandas as pd
from transformers import BertTokenizer


def test_prepare_data_for_testing():
    df_data = pd.read_csv("../../data/unit_test_data/test_data2.tsv", sep="\t")
    data = df_data['tweet']
    tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased')

    dataloader = prepare_data_for_testing(data, tokenizer, 256, 1)
    for step, batch in enumerate(dataloader):
        input_ids, input_mask = batch
        if step == 1:
            print(input_ids)

    dataloader2 = prepare_data_for_testing(data, tokenizer, 256, 1)
    for step, batch in enumerate(dataloader2):
        input_ids, input_mask = batch
        if step == 1:
            print(input_ids)


def test_relabel_data_to_binary():
    labels = ['OFF', 'NOT', 'nasilje']
    label_set_1 = ['nasilje', 'OFF']
    label_set_2 = ['NOT']

    labels = relabel_data_to_binary(labels, label_set_1, label_set_2)
    print(labels)
    label_set_1 = ['nasilje', 'off']
    label_set_2 = ['NOT']
    labels = relabel_data_to_binary(labels, label_set_1, label_set_2)


if __name__ == "__main__":
    test_relabel_data_to_binary()